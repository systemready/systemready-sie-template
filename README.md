# Template Structure for SystemReady SIE Compliance Reports
This repo structure is the template for collecting compliance evidence for a
SystemReady SIE certification.

## Overview

The Security Interface Extension is an extension to the certification received for the SystemReady SR, ES, and IR bands. For a system to be considered for the Security Interface Extension certification, it must either already have certification or be in the certification process for one of the SystemReady bands.

- See the Arm SystemReady Requirements Specification for detailed information about the requirements for SystemReady certification: https://developer.arm.com/documentation/den0109/latest

- See the SystemReady Security Interface Extension User Guide for details on the SIE ACS test process, how to deploy an SIE ACS image, enroll Secure Boot keys, run the ACS tests, and collect the test results: https://developer.arm.com/documentation/102872/latest

## General Instructions

General instructions for collecting SystemReady SIE compliance logs:

### `./report.txt`
Fill in with information about the system being certified

### `./acs_results/SIE`
Place an entire copy of the `acs_results` partitions, as follows:

    SIE/sct_results/Overall/Summary.log
    SIE/fwts/FWTSResults.log

If the system supports host-based (in-band) firmware updates the following additional logs should be present in the `acs_results` partition:

    SIE/fwupdate/esrt_dump.log
    SIE/fwupdate/fmp_dump.log
    SIE/fwupdate/capsule_header.log
    SIE/fwupdate/fwupdate_tampered.log
    SIE/fwupdate/fwupdate.log
    SIE/fwupdate/fmp_post_update_dump.log

If the system has a TPM 2.0 the following additional logs should be present in the `acs_results` partition:

    SIE/tpm2/eventlog.log
    SIE/tpm2/pcr.log

### `./docs/`
Place any firmware or device documentation, manuals, user guides, build instructions, etc..

#### `./fw/capsule*.bin`
Place UEFI capsule binaries under `./fw/`, with a name matching `capsule*.bin`.

